package me.thecomputergeek2.coderunner.util;

public class StackTraceUtil {
    public static String[] stringifyStackTrace(StackTraceElement[] elements) {
        String[] ret = new String[elements.length];
        for (int i = 0; i < elements.length; i++) {
            ret[i] = elements[i].toString();
        }
        return ret;
    }
}
