package me.thecomputergeek2.coderunner;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public enum ScriptEngines {
    NASHORN("nashorn"),
    GROOVY("groovy")
    ;

    public final String name;

    ScriptEngines(String name) {
        this.name = name;
    }

    public ScriptEngine getEngine(ScriptEngineManager manager) {
        return manager.getEngineByName(this.name);
    }

}
