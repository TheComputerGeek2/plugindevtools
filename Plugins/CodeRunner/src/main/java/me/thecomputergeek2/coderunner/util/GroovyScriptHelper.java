package me.thecomputergeek2.coderunner.util;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.IOException;

public class GroovyScriptHelper {

    public static Object runScript(File scriptFile, Binding binding) {
        GroovyShell shell = new GroovyShell(binding);
        try {
            return shell.evaluate(scriptFile);
        } catch (IOException e) {
            return e;
        }
    }
    public static Object runScript(String script, Binding binding) {
        GroovyShell shell = new GroovyShell(binding);
        return shell.evaluate(script);
    }
}
