package me.thecomputergeek2.coderunner.cmd;

import me.thecomputergeek2.coderunner.PluginCodeRunner;
import me.thecomputergeek2.coderunner.ScriptContextSpec;
import me.thecomputergeek2.coderunner.ScriptEngines;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import static me.thecomputergeek2.coderunner.util.ContextHelper.createJSR223Bindings;

public class JSR223ScriptCommand extends JSR223CommandAbstract {

    public JSR223ScriptCommand(ScriptEngines lang) {
        super(lang);
    }

    @Override
    public boolean onCommandInner(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            String scriptName = args[0];
            File scriptFile = new File(PluginCodeRunner.i.scriptsFolder, scriptName);

            ScriptContextSpec ctx = createScriptContextSpec(sender);

            try {
                Object result = engine.eval(new FileReader(scriptFile), createJSR223Bindings(ctx));
                sender.sendMessage(result.toString());
            } catch (FileNotFoundException e) {
                sender.sendMessage("Script file not found");
                return false;
            } catch (ScriptException e) {
                handleScriptException(e, sender);
            }
            return true;
        }
        return false;
    }
}
