package me.thecomputergeek2.coderunner;

import org.bukkit.Bukkit;

import java.util.LinkedHashMap;

public class ScriptContextSpec extends LinkedHashMap<String, Object> {

    public void applyGlobalBinds() {
        this.put("server", Bukkit.getServer());
    }

}
