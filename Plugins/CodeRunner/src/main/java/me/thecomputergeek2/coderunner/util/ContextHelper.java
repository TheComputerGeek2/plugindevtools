package me.thecomputergeek2.coderunner.util;

import me.thecomputergeek2.coderunner.ScriptContextSpec;

import javax.script.SimpleBindings;
import java.util.Map;

public class ContextHelper {
    public static void applyContext(ScriptContextSpec source, javax.script.Bindings target) {
        target.putAll(source);
    }

    public static javax.script.Bindings createJSR223Bindings(ScriptContextSpec source) {
        javax.script.Bindings ret = new SimpleBindings();
        applyContext(source, ret);
        return ret;
    }

    public static void applyContext(ScriptContextSpec source, groovy.lang.Binding target) {
        for (Map.Entry<String, Object> entry : source.entrySet()) {
            target.setVariable(entry.getKey(), entry.getValue());
        }
    }

    public static groovy.lang.Binding createGroovyBinding(ScriptContextSpec source) {
        groovy.lang.Binding ret = new groovy.lang.Binding();
        applyContext(source, ret);
        return ret;
    }

}
