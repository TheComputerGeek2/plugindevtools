package me.thecomputergeek2.coderunner.cmd;

import me.thecomputergeek2.coderunner.PluginCodeRunner;
import me.thecomputergeek2.coderunner.ScriptContextSpec;
import me.thecomputergeek2.coderunner.ScriptEngines;
import me.thecomputergeek2.coderunner.exception.RequirementNotMetException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import javax.script.ScriptException;

import static me.thecomputergeek2.coderunner.util.ContextHelper.createJSR223Bindings;

public class JSR223ExecCommand extends JSR223CommandAbstract {

    public JSR223ExecCommand(ScriptEngines lang) {
        super(lang);
    }

    @Override
    public boolean onCommandInner(CommandSender sender, Command command, String label, String[] args) throws Exception {
        if (!PluginCodeRunner.config.areInlineScriptsEnabled()) {
            throw new RequirementNotMetException("Inline scripts are not enabled");
        }
        String script = String.join(" ", args);
        if (script.trim().isEmpty()) {
            sender.sendMessage("You didn't pass any script instructions");
            return false;
        }

        ScriptContextSpec ctx = createScriptContextSpec(sender);

        try {
            Object result = engine.eval(script, createJSR223Bindings(ctx));
            sender.sendMessage(result.toString());
        } catch (ScriptException ex) {
            handleScriptException(ex, sender);
        }
        return true;
    }
}
