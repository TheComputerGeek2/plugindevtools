package me.thecomputergeek2.coderunner.cmd;

import me.thecomputergeek2.coderunner.PluginCodeRunner;
import me.thecomputergeek2.coderunner.ScriptContextSpec;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.io.File;

import static me.thecomputergeek2.coderunner.util.ContextHelper.createGroovyBinding;
import static me.thecomputergeek2.coderunner.util.GroovyScriptHelper.runScript;

public class CommandGroovy extends CmdBase {
    @Override
    public boolean onCommandInner(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            String scriptName = args[0];
            File scriptFile = new File(PluginCodeRunner.i.scriptsFolder, scriptName);

            ScriptContextSpec ctx = new ScriptContextSpec();
            ctx.applyGlobalBinds();
            ctx.put("me", sender);

            Object result = runScript(scriptFile, createGroovyBinding(ctx));
            sender.sendMessage(result == null ? "null" : result.toString());
            return true;
        }

        sender.sendMessage("I don't this you used this right");
        return false;
    }
}
