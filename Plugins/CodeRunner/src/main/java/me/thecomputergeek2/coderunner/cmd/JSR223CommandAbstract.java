package me.thecomputergeek2.coderunner.cmd;

import me.thecomputergeek2.coderunner.PluginCodeRunner;
import me.thecomputergeek2.coderunner.ScriptContextSpec;
import me.thecomputergeek2.coderunner.ScriptEngines;
import org.bukkit.command.CommandSender;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import static me.thecomputergeek2.coderunner.util.StackTraceUtil.stringifyStackTrace;

public abstract class JSR223CommandAbstract extends CmdBase {

    protected ScriptEngine engine;

    public JSR223CommandAbstract(ScriptEngines lang) {
        this.engine = lang.getEngine(PluginCodeRunner.i.engineManager);
    }

    protected ScriptContextSpec createScriptContextSpec(CommandSender sender) {
        ScriptContextSpec spec = new ScriptContextSpec();
        configureScriptContextSpec(spec, sender);
        return spec;
    }

    protected void configureScriptContextSpec(ScriptContextSpec spec, CommandSender sender) {
        spec.applyGlobalBinds();
        spec.put("me", sender);
    }

    protected void handleScriptException(ScriptException exception, CommandSender sender) {
        sender.sendMessage("Script execution failed");
        sender.sendMessage(exception.getMessage());
        sender.sendMessage(stringifyStackTrace(exception.getStackTrace()));
    }

}
