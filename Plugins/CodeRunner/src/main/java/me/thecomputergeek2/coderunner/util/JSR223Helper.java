package me.thecomputergeek2.coderunner.util;

import org.bukkit.Bukkit;

import javax.script.Bindings;
import javax.script.ScriptEngineManager;

public class JSR223Helper {

    public static ScriptEngineManager createEngineManager() {
        ScriptEngineManager manager = new ScriptEngineManager();
        Bindings bindings = manager.getBindings();
        bindings.put("server", Bukkit.getServer());
        return manager;
    }
}
