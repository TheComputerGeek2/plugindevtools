package me.thecomputergeek2.coderunner.cmd;

import me.thecomputergeek2.coderunner.PluginCodeRunner;
import me.thecomputergeek2.coderunner.ScriptContextSpec;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import static me.thecomputergeek2.coderunner.util.ContextHelper.createGroovyBinding;
import static me.thecomputergeek2.coderunner.util.GroovyScriptHelper.runScript;

public class CommandGroovyExec extends CmdBase {
    @Override
    public boolean onCommandInner(CommandSender sender, Command command, String label, String[] args) {
        if (!PluginCodeRunner.config.areInlineScriptsEnabled()) {
            sender.sendMessage(label + " is not enabled for use.");
            return false;
        }
        String script = String.join(" ", args);
        if (script.trim().isEmpty()) {
            sender.sendMessage("You did not specify anything to execute.");
            return false;
        }

        ScriptContextSpec ctx = new ScriptContextSpec();
        ctx.applyGlobalBinds();
        ctx.put("me", sender);
        sender.sendMessage(runScript(script, createGroovyBinding(ctx)).toString());
        return true;
    }
}
