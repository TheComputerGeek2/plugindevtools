package me.thecomputergeek2.coderunner;

public class CodeRunnerConfig {

    private PluginCodeRunner plugin;

    public CodeRunnerConfig(PluginCodeRunner plugin) {
        this.plugin = plugin;
    }

    public boolean areInlineScriptsEnabled() {
        return plugin.getConfig().getBoolean("enable.commands.inlinescript", true);
    }
}
