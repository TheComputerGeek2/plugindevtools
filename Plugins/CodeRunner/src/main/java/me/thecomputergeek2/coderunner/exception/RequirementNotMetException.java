package me.thecomputergeek2.coderunner.exception;

public class RequirementNotMetException extends Exception {

    public RequirementNotMetException(String message) {
        super(message);
    }

}
