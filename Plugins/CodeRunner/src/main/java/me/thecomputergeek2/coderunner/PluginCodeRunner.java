package me.thecomputergeek2.coderunner;

import me.thecomputergeek2.coderunner.cmd.*;
import me.thecomputergeek2.coderunner.util.JSR223Helper;
import org.bukkit.plugin.java.JavaPlugin;

import javax.script.ScriptEngineManager;
import java.io.File;

public class PluginCodeRunner extends JavaPlugin {

    public static PluginCodeRunner i;
    public static CodeRunnerConfig config;

    public File scriptsFolder;

    public ScriptEngineManager engineManager;

    @Override
    public void onEnable() {
        super.onEnable();

        i = this;

        this.saveDefaultConfig();
        config = new CodeRunnerConfig(this);

        File dataFolder = this.getDataFolder();
        if (!dataFolder.exists()) {
            dataFolder.mkdirs();
        }

        this.scriptsFolder = dataFolder;

        this.engineManager = JSR223Helper.createEngineManager();

        this.getCommand("groovy").setExecutor(new CommandGroovy());
        this.getCommand("groovyExec").setExecutor(new CommandGroovyExec());
        this.getCommand("nashexec").setExecutor(new JSR223ExecCommand(ScriptEngines.NASHORN));
        this.getCommand("nash").setExecutor(new JSR223ScriptCommand(ScriptEngines.NASHORN));
        this.getCommand("gr223").setExecutor(new JSR223ScriptCommand(ScriptEngines.GROOVY));
        this.getCommand("gr223exec").setExecutor(new JSR223ExecCommand(ScriptEngines.GROOVY));
        this.getCommand("coderunner").setExecutor(new CommandCodeRunnerManage());
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

}
