package me.thecomputergeek2.coderunner.cmd;

import me.thecomputergeek2.coderunner.exception.RequirementNotMetException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import javax.script.ScriptException;

public abstract class CmdBase implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            return onCommandInner(sender, command, label, args);
        } catch (RequirementNotMetException e) {
            sender.sendMessage(e.getMessage());
        } catch (ScriptException e) {
            e.printStackTrace();
            // TODO handle
        } catch (Exception e) {
            e.printStackTrace();
            sender.sendMessage(e.getMessage());
            // TODO do a better job with this
        }
        return false;
    }

    public abstract boolean onCommandInner(CommandSender sender, Command command, String label, String[] args) throws Exception;
}
