package me.thecomputergeek2.coderunner.cmd;

import me.thecomputergeek2.coderunner.PluginCodeRunner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class CommandCodeRunnerManage extends CmdBase {
    @Override
    public boolean onCommandInner(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            // reload the config
            PluginCodeRunner.i.reloadConfig();
            sender.sendMessage("Config reloaded");
            return true;
        }
        return false;
    }
}
