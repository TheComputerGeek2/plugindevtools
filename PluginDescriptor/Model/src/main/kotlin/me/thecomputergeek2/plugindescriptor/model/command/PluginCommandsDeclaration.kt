package me.thecomputergeek2.plugindescriptor.model.command

import me.thecomputergeek2.plugindescriptor.model.ConfigurableList
import me.thecomputergeek2.plugindescriptor.model.FavoredMutableList

/**
 *
 */
class PluginCommandsDeclaration:
        FavoredMutableList<PluginCommandModel>(),
        ConfigurableList<PluginCommandModel>
{

    override fun createElement(): PluginCommandModel {
        return PluginCommandModel()
    }

}
