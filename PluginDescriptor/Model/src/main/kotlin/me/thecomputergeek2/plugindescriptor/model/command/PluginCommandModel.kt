package me.thecomputergeek2.plugindescriptor.model.command

import me.thecomputergeek2.plugindescriptor.model.*

/**
 * @param name
 * @param aliases
 * @param requiredPermission
 * @param noPermissionMessage
 * @param usageDescription explains how to use the command when onCommand does not return true.
 */
data class PluginCommandModel(
        var name: CommandName? = null,
        var aliases: MutableList<CommandAlias> = mutableListOf(),
        var requiredPermission: PermissionNodeId? = null,
        var noPermissionMessage: CommandNoPermissionMessage? = null,
        var usageDescription: CommandUsageMessage? = null
)
