package me.thecomputergeek2.plugindescriptor.model

/**
 * A list of plugin names.
 */
class PluginIdList:
        MutableList<PluginName>,
        FavoredMutableList<PluginName>()
