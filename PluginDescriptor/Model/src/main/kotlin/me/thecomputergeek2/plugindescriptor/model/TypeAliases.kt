package me.thecomputergeek2.plugindescriptor.model

typealias PluginName = String
typealias PluginAuthor = String
typealias PluginVersion = String
typealias PluginDescription = String
typealias PluginApiVersion = String
typealias PluginWebsite = String?
typealias PluginLoggingPrefix = String?
typealias PermissionNodeId = String
typealias PermissionDescription = String
typealias CommandName = String
typealias CommandAlias = String
typealias CommandUsageMessage = String
typealias CommandNoPermissionMessage = String
typealias PluginMainClass = String

/**
 * A helper type which describes an action with receiver.
 *
 * @param E the receiver type.
 */
typealias Config<E> = E.()->Unit

/**
 * The preferred type to use for MutableList implementations.
 *
 * @param E the inner type for the list.
 */
typealias FavoredMutableList<E> = ArrayList<E>
