package me.thecomputergeek2.plugindescriptor.model

/**
 * @param primaryAuthor
 * @param authors
 */
class AuthoringData(
        var primaryAuthor: PluginAuthor? = null,
        authors: Iterable<PluginAuthor> = emptyList()
): MutableList<PluginAuthor>,
        FavoredMutableList<PluginAuthor>()
{

    init {
        addAll(authors)
    }

}
