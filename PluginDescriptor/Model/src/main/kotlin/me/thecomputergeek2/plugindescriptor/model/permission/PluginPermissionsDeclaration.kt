package me.thecomputergeek2.plugindescriptor.model.permission

import me.thecomputergeek2.plugindescriptor.model.ConfigurableList
import me.thecomputergeek2.plugindescriptor.model.FavoredMutableList

/**
 *
 */
class PluginPermissionsDeclaration:
        FavoredMutableList<PluginPermissionModel>(),
        ConfigurableList<PluginPermissionModel>
{

    override fun createElement(): PluginPermissionModel {
        return PluginPermissionModel()
    }

}
