package me.thecomputergeek2.plugindescriptor.model

import me.thecomputergeek2.plugindescriptor.model.command.PluginCommandsDeclaration
import me.thecomputergeek2.plugindescriptor.model.permission.PluginPermissionsDeclaration

/**
 * A representation of a plugin descriptor file, better known as `plugin.yml`.
 * See [Spigotmc's wiki page](https://www.spigotmc.org/wiki/plugin-yml/)
 *
 * @param name the name for the plugin. This is required to be defined for a valid descriptor.
 * @param version the plugin's version. This is required to be defined for a valid descriptor.
 * @param mainClass the main class of the plugin. This is required to be defined for a valid descriptor.
 * @param authors the authors of the plugin.
 * @param description the plugin's description.
 * @param dependsOn the names of plugins this plugin depends on to load.
 * @param apiVersion The version of the API this plugin uses.
 * @param commands a declaration of commands that should be setup for this plugin.
 * @param database does this plugin wish to use the database defined in bukkit.yml?
 *   Defaults to false and is non trivial to make use of.
 *   See [Plugin Databases](https://bukkit.gamepedia.com/Plugin_Databases)
 * @param loadBefore the names of plugins that this should load before even if they don't declare a relation to this plugin themselves.
 * @param loadTime when this plugins should be loaded by the server, defaults to POSTWORLD.
 * @param permissions a declaration of permissions that should be setup for this plugin.
 * @param prefix a prefix to use when logging instead of the plugin's name.
 * @param softDependencies a list of plugins that should be loaded before this one to ensure full functionality, but are not required.
 * @param website the website to list as this project's home page.
 */
data class PluginDescriptorModel(
        var name: PluginName = "",
        var version: PluginVersion = "",
        var mainClass: PluginMainClass = "",
        var authors: AuthoringData = AuthoringData(),
        var description: PluginDescription = "",
        var apiVersion: PluginApiVersion = "",
        var loadTime: PluginLoadTime = PluginLoadTime.POSTWORLD,
        var loadBefore: PluginIdList = PluginIdList(),
        var dependsOn: PluginIdList = PluginIdList(),
        var softDependencies: PluginIdList = PluginIdList(),
        var prefix: PluginLoggingPrefix = null,
        var commands: PluginCommandsDeclaration = PluginCommandsDeclaration(),
        var permissions: PluginPermissionsDeclaration = PluginPermissionsDeclaration(),
        var database: Boolean? = null,
        var website: PluginWebsite = null
)
