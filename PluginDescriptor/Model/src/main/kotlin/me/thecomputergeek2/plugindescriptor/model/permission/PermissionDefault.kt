package me.thecomputergeek2.plugindescriptor.model.permission

/**
 *
 */
enum class PermissionDefault {

    /**
     * Signifies that everyone should have this permission by default.
     */
    TRUE,

    /**
     * Signifies that nobody should have this permission by default.
     */
    FALSE,

    /**
     * Signifies that only server operators should have this permission by default.
     */
    OP,

    /**
     * Signifies that only non-operators should have this permission by default.
     */
    NOT_OP

}
