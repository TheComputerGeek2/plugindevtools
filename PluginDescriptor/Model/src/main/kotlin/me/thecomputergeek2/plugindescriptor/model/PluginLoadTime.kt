package me.thecomputergeek2.plugindescriptor.model

/**
 * Phases of server setup for plugins to be loaded.
 */
enum class PluginLoadTime {

    /**
     * Plugin should be loaded before world generation occurs.
     */
    STARTUP,

    /**
     * Plugin should be loaded after world generation happens.
     * This is the default load time for plugins.
     */
    POSTWORLD
}
