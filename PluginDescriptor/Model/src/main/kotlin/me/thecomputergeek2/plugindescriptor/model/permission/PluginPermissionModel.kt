package me.thecomputergeek2.plugindescriptor.model.permission

import me.thecomputergeek2.plugindescriptor.model.PermissionDescription
import me.thecomputergeek2.plugindescriptor.model.PermissionNodeId

/**
 * The model of a permission within a plugin.yml file.
 *
 * @param nodeId the string that is used to reference this permission. Required in order to function.
 * @param children
 * @param defaultValue specifies what user groups should have this permission by default.
 *   If not defined, defaults to operators only.
 * @param description a description for the permission,
 *   often helpful to have for other plugins that may access permission information programmatically.
 */
data class PluginPermissionModel(
        var nodeId: PermissionNodeId? = null,
        var description: PermissionDescription? = null,
        var defaultValue: PermissionDefault = PermissionDefault.OP,
        var children: MutableMap<PermissionNodeId, Boolean> = mutableMapOf()
)
