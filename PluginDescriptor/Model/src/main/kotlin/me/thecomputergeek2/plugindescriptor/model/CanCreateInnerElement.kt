package me.thecomputergeek2.plugindescriptor.model

/**
 * A utility interface for ensuring that a type can create elements of the type that it holds.
 *
 * @param E the type of the inner element.
 */
interface CanCreateInnerElement<E> {

    /**
     * Create an instance of an element to be added to a collection.
     *
     * @return the newly created element.
     */
    fun createElement(): E

}
