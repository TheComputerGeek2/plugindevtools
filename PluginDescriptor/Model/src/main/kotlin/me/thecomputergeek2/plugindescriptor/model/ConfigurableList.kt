package me.thecomputergeek2.plugindescriptor.model

/**
 * @param E the inner element type.
 */
interface ConfigurableList<E>: MutableList<E>, CanCreateInnerElement<E> {

    /**
     * Creates a new instance of E and then applies config to it then adds it to this collection.
     *
     * @param config the configuration to apply to a new instance of E before adding it to this list.
     * @return true if the element was successfully added.
     */
    fun add(config: Config<E>): Boolean {
        val element: E = createElement()
        element.config()
        return add(element)
    }

    /**
     * Creates a new instance of E and then applies config to it then adds it to this collection.
     *
     * @param config the configuration to apply to a new instance of E before adding it to this list.
     */
    operator fun plusAssign(config: Config<E>) {
        add(config)
    }

}
