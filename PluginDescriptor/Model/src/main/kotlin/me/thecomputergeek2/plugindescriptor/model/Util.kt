package me.thecomputergeek2.plugindescriptor.model

/**
 * Allows any type to be configured by invoking with a lambda passed for easier DSL construction.
 *
 * @param config the configuration block to apply.
 * @param T The receiver type.
 */
operator fun <T> T.invoke(config: Config<T>) {
    this.config()
}
