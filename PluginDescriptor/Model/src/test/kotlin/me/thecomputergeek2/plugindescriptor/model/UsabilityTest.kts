import me.thecomputergeek2.plugindescriptor.model.*
import me.thecomputergeek2.plugindescriptor.model.permission.PermissionDefault
import me.thecomputergeek2.plugindescriptor.model.permission.PluginPermissionModel

val model = PluginDescriptorModel()
model {
    name = "CodeRunner"
    version = "1.0-SNAPSHOT"

    loadTime = PluginLoadTime.STARTUP

    authors {
        this += ("TheComputerGeek2")
        primaryAuthor = "TheComputerGeek2"
    }

    permissions {
        this += {
            nodeId = "my.node.id"
            defaultValue = PermissionDefault.OP
            children {
                //
            }
        }
        this += PluginPermissionModel()
        add {
            nodeId = "my.node.id.2"
        }
    }

    commands {
        //
    }
}
