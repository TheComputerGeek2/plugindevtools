package me.thecomputergeek2.plugindescriptor.model.validator

import me.thecomputergeek2.plugindescriptor.model.PluginDescriptorModel

class PluginDescriptorValidator(
        private val model: PluginDescriptorModel
) {

    /**
     * A collection of issues discovered by this validator
     */
    private val issues: MutableList<String> = mutableListOf()

    fun runValidations() {
        validatePluginName()
        validatePluginVersion()
        validatePluginMainClass()
        validatePluginAuthors()
        validatePluginDescription()
        validatePluginApiVersion()
        validatePluginLoadTime()
        validatePluginLoadBefore()
        validatePluginDependsOn()
        validatePluginSoftDependencies()
        validatePluginPrefix()
        validatePluginCommands()
        validatePluginPermissions()
        validatePluginDatabase()
        validatePluginWebsite()
    }

    fun validatePluginName() {
        demandFieldMatches(model.name, pluginNamePattern, "name");
        //if (!pluginNamePattern.matches(model.name)) issues += "Name violates pattern: ${pluginNamePattern.pattern}"
    }

    fun validatePluginVersion() {
        demandFieldMatches(model.version, pluginVersionRegex, "version")
    }

    fun validatePluginMainClass() {
        TODO()
    }

    fun validatePluginAuthors() {
        TODO()
    }

    fun validatePluginDescription() {
        demandFieldMatches(model.description, pluginDescriptionRegex, "description");
    }

    fun validatePluginApiVersion() {
        demandFieldMatches(model.apiVersion, apiVersionRegex, "ApiVersion")
    }

    fun validatePluginLoadTime() {
        // no op here, I think?
    }

    fun validatePluginLoadBefore() {
        TODO()
    }

    fun validatePluginDependsOn() {
        TODO()
    }

    fun validatePluginSoftDependencies() {
        TODO()
    }

    fun validatePluginPrefix() {
        model.prefix?.let { demandFieldMatches(it, prefixRegex, "prefix") }
    }

    fun validatePluginCommands() {
        TODO()
    }

    fun validatePluginPermissions() {
        TODO()
    }

    fun validatePluginDatabase() {
        // no op here, I think?
    }

    fun validatePluginWebsite() {
        model.website?.let { demandFieldMatches(it, websiteRegex, "website") }
    }

    private fun demandFieldMatches(value: String, regex: Regex, element: String) {
        if (!regex.matches(value)) issues += "${element.capitalize()} violates pattern: ${regex.pattern}"
    }

    private companion object Patterns {
        val websiteRegex: Regex = TODO()
        val prefixRegex: Regex = TODO()
        val apiVersionRegex: Regex = TODO()
        val pluginVersionRegex: Regex = TODO()
        val pluginNamePattern: Regex = Regex("[a-zA-Z0-9_]+")
        val pluginDescriptionRegex: Regex = TODO()
    }

}
